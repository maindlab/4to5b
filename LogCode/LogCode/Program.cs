﻿using System;

namespace LogCode
{
  internal class Program
  {
    public static void Main(string[] args)
    {
      Console.WriteLine("intput ur string");
      var line = Console.ReadLine();
      line = line.Replace(" ", String.Empty);
      if (line.Length % 4 != 0)
      {
        Console.WriteLine("ur line isn't multiple to 4");
        return;
      }
      string result = string.Empty;
      var lib = new Lib();
      for (int i = 0; i < line.Length / 4; i++)
      {
        result += lib.Get(line.Substring(i * 4, 4));
      }

      Console.WriteLine("your code:");
      Console.WriteLine(result);
      Console.WriteLine("by Roman Sorokin1");
    }
  }
}